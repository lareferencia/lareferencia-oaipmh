#!/bin/bash 

mvn clean package
sudo /etc/init.d/tomcat8 stop
sudo cp target/xoai-2.0.war /var/lib/tomcat8/webapps/oai.war
sudo rm -r /var/lib/tomcat8/webapps/oai
sudo /etc/init.d/tomcat8 start
